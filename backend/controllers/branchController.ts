import { Request, Response } from 'express';

const dao = require('../dao/branch.dao');

//export function create(req: Request, res: Response){
export function create(req: Request, res: Response){
    dao.create(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function get(req: Request, res: Response){
    dao.get(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function update(req: Request, res: Response){
    dao.update(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function disable(req: Request, res: Response){
    dao.disable(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function getAll(req: Request, res: Response){
    dao.getAll(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function addGroup(req: Request, res: Response){
    dao.addGroup(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function addBoss(req: Request, res: Response){
    dao.addBoss(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}
