import { Request, Response } from 'express';

const dao = require('../dao/member.dao');

//export function create(req: Request, res: Response){
export function create(req: Request, res: Response){
    dao.create(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function get(req: Request, res: Response){
    dao.get(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function update(req: Request, res: Response){
    dao.update(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function disable(req: Request, res: Response){
    dao.deleteF(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function getAll(req: Request, res: Response){
    dao.getAll(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}