import { Request, Response } from 'express';

const dao = require('../dao/group.dao');

//export function create(req: Request, res: Response){
export function create(req: Request, res: Response){
    dao.create(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function get(req: Request, res: Response){
    dao.get(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function update(req: Request, res: Response){
    dao.update(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function disable(req: Request, res: Response){
    dao.disable(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function getAll(req: Request, res: Response){
    dao.getAll(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function addToBranch(req: Request, res: Response){
    dao.assignBranch(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function assignMonitor(req: Request, res: Response){
    dao.assignMonitor(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function addMember(req: Request, res: Response){
    dao.addMember(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}

export function getMembers(req: Request, res: Response){
    dao.getMembers(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}