import { Request, Response } from 'express';

const dao = require('../dao/login.dao');

//export function create(req: Request, res: Response){
export function login(req: Request, res: Response){
    dao.login(req)
    .then((result: any) =>{
        res.send({
            result
        })
    })
    .catch((err: any)=>{
        console.log(err);
        res.send(err);
    })
}