import express from 'express';
const mongoose = require('mongoose');
const cors = require('cors');

import { memberRouter } from './routes/member_router'
import { movementRouter } from './routes/movement_router'
import { advisorRouter } from './routes/advisor_router'
import { zoneRouter } from './routes/zone_router'
import { branchRouter } from './routes/branch_router'
import { groupRouter } from './routes/group_router'
import { loginRouter } from './routes/login_router'

const app = express();
const PORT = 5000;

app.use(cors());
app.use(express.json());

const uri = 'mongodb+srv://us0:us0@cluster0.m1ukt.mongodb.net/Pro0?retryWrites=true&w=majority';
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true , useUnifiedTopology: true} );
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
});
 
app.use((err:any, req:any, res:any, next:any)=>{
  console.error(err);
  res.send(err);
})
 
app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});

app.use('/member',    memberRouter);
app.use('/movement',  movementRouter);
app.use('/advisor',   advisorRouter);
app.use('/zone',      zoneRouter);
app.use('/branch',    branchRouter);
app.use('/group',     groupRouter);
app.use('/login',     loginRouter);