
const Person = require('../models/Person')
const Member = require('../models/Member');
const { resolve } = require('path');

function create(req){
    const name = req.body.name;
    const id = req.body.id;
    const number = req.body.number;
    const email = req.body.email;
    const district = req.body.district;

    const state = req.body.state;
    const position = req.body.position;
    const group = "";
    
    const get_id = req.body.id;
    return new Promise((resolve, reject) =>{
        Member.find({"id":{$eq : get_id}})
        .then(member => {
            if(member.length == 0){
                const newPerson = new Person({name, id, number, email, district});
                const newMember = new Member({state, position, id, group});
                newPerson.save();
                newMember.save();
                resolve({status: "ok", message: "Member has been added"})
                reject(err)
            }
            else{
                resolve({status: "repeated member", message: "Member already exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function get(req){
    const get_id = req.body.id;
    return new Promise((resolve, reject) =>{
        Member.find({"id":{$eq : get_id}})
        .then(member => {
            resolve(member)
        })
        .catch(err => reject(err))
    })
}

function getById(id){
    return new Promise((resolve, reject) =>{
        Member.find({"id": id})
        .then(member => {
            resolve(member)
        })
        .catch(err => reject(err))
    })
}

function getAll(){
    return new Promise((resolve, reject) => {
        resolve(Person.find())
    })
}

function update(req){
    const name = req.body.name;
    const get_id = req.body.id;
    const number = req.body.number;
    const email = req.body.email;
    const district = req.body.district;

    const state = req.body.state;
    const position = req.body.position;
    const group = req.body.group;

    return new Promise((resolve, reject) =>{
        Member.find({"id":{$eq : get_id}})
        .then(member => {
            if(member.length > 0){
                Member.update(
                    {"id":{$eq : get_id}},
                    {
                        $set:{
                            "state" : state,
                            "position":position,
                            "group":group
                        }
                    }
                );
                Person.update(
                    {"id":{$eq : get_id}},
                    {
                        $set:{
                            "name" : name,
                            "number":number,
                            "email":email,
                            "district":district
                        }
                    }
                );
                resolve("Member has been updated");
                reject(err => err);
            }
            else{
                resolve({status: "unknown member", message: "Member does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function deleteF(req){
    const get_id = req.body.id;

    return new Promise((resolve, reject) =>{
        Member.find({"id":{$eq : get_id}})
        .then(member => {
            if(member.length > 0){
                Member.update(
                    {"id":{$eq : get_id}},
                    {
                        $set:{ "state" : "inactive"}
                    }
                );
                resolve("Member has been deactivated");
                reject(err => err);
            }
            else{
                resolve({status: "unknown member", message: "Member does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function changeRole(user_id, role){
    return new Promise((resolve, reject) =>{
        Member.update(
            {"id": user_id},
            {$set:{ "position": role } }
        )
        .then(res => {
            resolve({status: "user updated", message: "Updated role"})
        })
        .catch(err => reject(err))
    })
}

function getByGroup(group){
    return new Promise((resolve, reject) =>{
        Member.find(
            {"id": user_id},
            {$set:{ "position": role } }
        )
        .then(res => {
            resolve({status: "user updated", message: "Updated role"})
        })
        .catch(err => reject(err))
    })
}

module.exports = {
    create,
    get,
    getById,
    getAll,
    update,
    deleteF,
    changeRole
}