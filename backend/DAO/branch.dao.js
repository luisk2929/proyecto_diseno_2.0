

const Branch = require('../models/Branch')
const group = require('../dao/group.dao')

function create(req){
    const name = req.body.name;
    const leaders = [];
    const groups = [];
    const hasZone = false;
    const state = req.body.state;
    
    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Branch.find({"name":{$eq : get_id}})
        .then(branch => {
            if(branch.length == 0){
                const newBranch = new Branch({name, leaders, groups, hasZone, state});
                newBranch.save();
                resolve({status: "ok", message: "Branch has been added"})
                reject(err)
            }
            else{
                resolve({status: "repeated branch", message: "Branch already exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function get(req){
    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Branch.find({"name": {$eq : get_id}})
        .then(branch => {
            resolve(branch)
        })
        .catch(err => reject(err))
    })
}

function getAll(){
    return new Promise((resolve, reject) => {
        resolve(Branch.find())
    })
}

function update(req){
    const name = req.body.name;
    const state = req.body.state;

    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Branch.find({"name":{$eq : get_id}})
        .then(branch => {
            if(branch.length > 0){
                Person.update(
                    {"name":{$eq : get_id}},
                    {
                        $set:{
                            "name" : name,
                            "state": state
                        }
                    }
                );
            }
            else{
                resolve({status: "invalid branch", message: "Branch does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function disable(req){
    const get_id = req.body.name;

    return new Promise((resolve, reject) =>{
        Branch.find({"name":{$eq : get_id}})
        .then(branch => {
            if(branch.length > 0){
                Branch.update(
                    {"id":{$eq : get_id}},
                    {
                        $set:{ "state" : "inactive"}
                    }
                );
                resolve("Branch has been deactivated");
                reject(err => err);
            }
            else{
                resolve({status: "unknown branch", message: "Branch does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function getById(name){
    return new Promise((resolve, reject) =>{
        Branch.find({"name": {$eq : name}})
        .then(branch => {
            resolve(branch)
        })
        .catch(err => reject(err))
    })
}

function addGroup(req){
    const name = req.body.name;     //branch
    const grupo = req.body.zone;    //grupo
    console.log('hi')
    return new Promise((resolve, reject) =>{
        getById(name)
        .then(result =>{
            if(result.length > 0){
                group.getById(grupo)
                .then(res => {
                    if(res.length > 0){
                        Zone.update(
                            {"name":{$eq : name}},
                            {
                                $groups:{
                                    res: _id
                                }
                            }
                        );
                    resolve({status: "Group added", message: "Group has been added to branch"})
                    }
                    else{
                        resolve({status: "invalid group", message: "Group does not exist"})
                    }
                })
            }
            else{
                resolve({status: "invalid branch", message: "Branch does not exist"})
            }
        })
    })
}

function addBoss(req){
    const name = req.body.name;     //rama
    const id = req.body.id;         //boss id

    return new Promise((resolve, reject) =>{
        Branch.find({"name": get_id}, {leaders: 1, _id: 0})
        .then(leaders => {
            console.log(leaders)
            if(leaders.length <= 2){
                Branch.update(
                    {"name":{$eq : name}},
                    {
                        $push:{
                            leaders : id
                        }
                    }
                );
                member.changeRole(id, "Branch leader")
                .then(res =>{
                    resolve(res);
                })
                resolve({status: "Boss added", message: "Boss has been added to branch"})
            }
            else{
                resolve({status: "invalid insert", message: "There can only be two leaders per zone"})
            }
        })
        .catch(err => reject(err))
    })
}

module.exports = {
    create,
    get,
    getAll,
    update,
    disable,
    addGroup,
    addBoss
}