

const Advisor = require('../models/GeneralAdvisor')

function create(req){
    const id = req.body.id;
    const initPeriod = req.body.initPeriod;
    const endPeriod = req.body.endPeriod;
    const username = req.body.username;
    const password = req.body.password;
    const status = req.body.status;
    
    const get_id = req.body.id;
    return new Promise((resolve, reject) =>{
        Advisor.find({"id":{$eq : get_id}})
        .then(advisor => {
            if(advisor.length == 0){
                const newAdvisor = new Advisor({id, initPeriod, endPeriod, username, password, status});
                newAdvisor.save();
                resolve({status: "ok", message: "Advisor has been added"})
                reject(err)
            }
            else{
                resolve({status: "repeated advisor", message: "Advisor already exists"})
            }
        })
        .catch(err => reject(err))
    })
}


function get(req){
    const get_id = req.body.id;
    return new Promise((resolve, reject) =>{
        Advisor.find({"id":{$eq : get_id}})
        .then(advisor => {
            resolve(advisor)
        })
        .catch(err => reject(err))
    })
}

function getAll(){
    return new Promise((resolve, reject) => {
        resolve(Advisor.find())
    })
}

function update(req){
    const id = req.body.id;
    const initPeriod = req.body.initPeriod;
    const endPeriod = req.body.endPeriod;
    const username = req.body.username;
    const password = req.body.password;
    const status = req.body.status;

    return new Promise((resolve, reject) =>{
        Advisor.find({"id":{$eq : id}})
        .then(advisor => {
            if(advisor.length > 0){
                Advisor.update(
                    {"id":{$eq : id}},
                    {
                        $set:{
                            "initPeriod" : initPeriod,
                            "endPeriod": endPeriod,
                            "username": username,
                            "password": password,
                            "status": status
                        }
                    }
                );
                resolve("Advisor has been updated");
                reject(err => err);
            }
            else{
                resolve({status: "unknown advisor", message: "Advisor does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function disable(req){
    const get_id = req.body.id;

    return new Promise((resolve, reject) =>{
        Advisor.find({"id":{$eq : get_id}})
        .then(advisor => {
            if(advisor.length > 0){
                Advisor.update(
                    {"id":{$eq : get_id}},
                    {
                        $set:{ "state" : "inactive"}
                    }
                );
                resolve("Advisor has been deactivated");
                reject(err => err);
            }
            else{
                resolve({status: "unknown advisor", message: "Advisor does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

module.exports = {
    create,
    get,
    getAll,
    update,
    disable
}