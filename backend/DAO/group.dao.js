

const Group = require('../models/Group')
const Monitor = require('../dao/member.dao')
const member = require('../dao/member.dao')
const { response } = require('express')

function existsMonitor(id){
    return new Promise((resolve, reject) =>{
        Monitor.getById(id)
        .then(monitor => {
            if(monitor.length > 0){
                resolve(true)
            }
            else{
                resolve(false)    
            }
        })
    })
}

function create(req){
    const name = req.body.name;
    const members = [];
    const monitor = req.body.monitor;
    const hasBranch = false;
    const state = req.body.state;
    
    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Group.find({"name":{$eq : get_id}})
        .then(group => {
            if(group.length == 0){
                existsMonitor(monitor)
                .then(exists => {
                    if(exists){
                        const newGroup = new Group({name, members, monitor, hasBranch, state});
                        newGroup.save();
                        resolve({status: "ok", message: "Group has been added"})
                        reject(err)
                    }
                    else{
                        resolve({status: "invalid member", message: "Member cannot be found"})
                    }
                })
                .catch(err => reject(err))
                
            }
            else{
                resolve({status: "repeated group", message: "Group already exists"})
            }
        })
        .catch(err => reject(err))
    })
}


function get(req){
    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Group.find({"name": {$eq : get_id}})
        .then(group => {
            resolve(group)
        })
        .catch(err => reject(err))
    })
}

function getAll(){
    return new Promise((resolve, reject) => {
        resolve(Group.find())
    })
}

function update(req){
    const name = req.body.name;
    const state = req.body.state;

    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Group.find({"name":{$eq : get_id}})
        .then(group => {
            if(group.length > 0){
                Group.update(
                    {"name":{$eq : get_id}},
                    {
                        $set:{
                            "name" : name,
                            "state": state
                        }
                    }
                );
            }
            else{
                resolve({status: "invalid group", message: "Group does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function disable(req){
    const get_id = req.body.name;

    return new Promise((resolve, reject) =>{
        Group.find({"name":{$eq : get_id}})
        .then(group => {
            if(group.length > 0){
                Group.update(
                    {"id":{$eq : get_id}},
                    {
                        $set:{ "state" : "inactive"}
                    }
                );
                resolve("Group has been deactivated");
                reject(err => err);
            }
            else{
                resolve({status: "unknown group", message: "Group does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function addMember(req){
    const name = req.body.name;     //nombre del grupo
    const id = req.body.id;
    return new Promise((resolve, reject) =>{
        Group.find({"name": name})
        .then(group => {
            if(group.length > 0){
                Group.update(
                    {"name": name}, { $push:{ members: id } }
                )
                .then(resolve({status: "ok", message: "Member has been added to group"}))
            }
            else{
                resolve({status: "invalid group", message: "Group does not exists"});
            }
        })
        .catch(err => reject(err))
    })
}

function assignBranch(req){
    const branch = req.body.id;
    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Group.find( {"name": get_id},
                    {hasBranch: 1, _id: 0})
        .then(group => {
            if(group[0].hasBranch){
                resolve({
                    status: "unable to assign",
                    message: "Group already has a branch"
                })
            }
            else{
                Group.update(
                    {"name":{$eq : name}},
                    {
                        $set:{
                            hasBranch: true
                        }
                    }
                );
            }
        })
        .catch(err => reject(err))
    })
}

function assignMonitor(req){
    const monitor = req.body.monitor;
    const group = req.body.group;

    return new Promise((resolve, reject) =>{
        existsMonitor(monitor)
        .then(exists =>{
            if(exists){
                Group.find({"name":group})
                .then(g =>{
                    if(g.length > 0){
                        member.changeRole(monitor, "Monitor");
                        var old = Group.find( {"name": get_id}, {monitor: 1, _id: 0})
                        member.changeRole(old, "Member");
                    }
                    else{
                        resolve({status: "invalid group", message: "group not found"})
                    }
                })
            }
            else{
                resolve({status: "invalid memeber", message: "member was not found"})
            }
        })
    })
}

function getMembers(req){
    const group = req.body.name;
    return new Promise((resolve, reject) =>{
        Group.find({"name": group}, {members: 1, _id: 0})
        .then(members => {
            array = members[0].members;

            Promise.all(array.map(member.getById)).then((results) => {
                resolve(results)
            })
        })
        .catch(err => reject(err))
    })
}

module.exports = {
    create,
    get,
    getAll,
    update,
    disable,

    addMember,
    assignBranch,
    assignMonitor,
    getMembers
}