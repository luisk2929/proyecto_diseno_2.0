const Zone = require('../models/Zone')
const branch = require('../dao/branch.dao')
const member = require('../dao/member.dao')

function create(req){
    const name = req.body.name;
    const leaders = [];
    const branches = [];
    const hasMovement = false;
    const state = req.body.state;
    
    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Zone.find({"name":{$eq : get_id}})
        .then(zone => {
            if(zone.length == 0){
                const newZone = new Zone({name, leaders, branches, hasMovement, state});
                newZone.save();
                resolve({status: "ok", message: "Zone has been added"})
                reject(err)
            }
            else{
                resolve({status: "repeated zone", message: "Zone already exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function get(req){
    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Zone.find({"name": {$eq : get_id}})
        .then(zone => {
            resolve(zone)
        })
        .catch(err => reject(err))
    })
}

function getById(id){
    return new Promise((resolve, reject) =>{
        Zone.find({"name": {$eq : id}})
        .then(zone => {
            resolve(zone)
        })
        .catch(err => reject(err))
    })
}

function getAll(){
    return new Promise((resolve, reject) => {
        resolve(Zone.find())
    })
}

function update(req){
    const name = req.body.name;
    const state = req.body.state;

    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Zone.find({"name":{$eq : get_id}})
        .then(zone => {
            if(zone.length > 0){
                Zone.update(
                    {"name":{$eq : get_id}},
                    {
                        $set:{
                            "name" : name,
                            "state": state
                        }
                    }
                );
            }
            else{
                resolve({status: "invalid zone", message: "Zone does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function disable(req){
    const get_id = req.body.name;

    return new Promise((resolve, reject) =>{
        Zone.find({"name":{$eq : get_id}})
        .then(zone => {
            if(zone.length > 0){
                Zone.update(
                    {"id":{$eq : get_id}},
                    {
                        $set:{ "state" : "inactive"}
                    }
                );
                resolve("Zone has been deactivated");
                reject(err => err);
            }
            else{
                resolve({status: "unknown zone", message: "Zone does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function addBranch(req){
    const name = req.body.name;     //zona
    const rama = req.body.zone;   //branch

    return new Promise((resolve, reject) =>{
        getById(name)
        .then(result =>{
            if(result.length > 0){      //existe
                branch.getById(rama)
                .then(res => {
                    if(res.length > 0){
                        Zone.update(
                            {"name":{$eq : name}},
                            {
                                $branches:{
                                    res: _id
                                }
                            }
                        );
                    resolve({status: "Branch added", message: "Branch has been added to zone"})
                    }
                    else{
                        resolve({status: "invalid branch", message: "Branch does not exist"})
                    }
                })
            }
            else{
                resolve({status: "invalid zone", message: "Zone does not exist"})
            }
        })
    })
}

function addBoss(req){
    const name = req.body.name;     //zona
    const id = req.body.id;         //boss id

    return new Promise((resolve, reject) =>{
        Zone.find({"name": get_id}, {leaders: 1, _id: 0})
        .then(leaders => {
            console.log(leaders)
            if(leaders.length <= 2){
                Zone.update(
                    {"name":{$eq : name}},
                    {
                        $push:{
                            leaders: id
                        }
                    }
                );
                member.changeRole(id, "Zone leader")
                .then(res =>{
                    resolve(res);
                })
                resolve({status: "Boss added", message: "Boss has been added to zone"})
            }
            else{
                resolve({status: "invalid insert", message: "There can only be two leaders per zone"})
            }
        })
        .catch(err => reject(err))
    })
}


module.exports = {
    create,
    get,
    getAll,
    update,
    disable,

    getById,
    addBranch,
    addBoss
}