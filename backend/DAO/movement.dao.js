

const Movement = require('../models/Movement')
const zone = require('../dao/zone.dao')

function create(req){
    const name = req.body.name;
    const country = req.body.country;
    const leader = req.body.leader;
    const zones = [];
    const description = req.body.description;
    const state = req.body.state;
    
    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Movement.find({"name":{$eq : get_id}})
        .then(movement => {
            if(movement.length == 0){
                const newMovement = new Movement({name, country, leader, zones, description, state});
                newMovement.save();
                resolve({status: "ok", message: "Movement has been added"})
                reject(err)
            }
            else{
                resolve({status: "repeated movement", message: "Movement already exists"})
            }
        })
        .catch(err => reject(err))
    })
}


function get(req){
    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Movement.find({"name": {$eq : get_id}})
        .then(movement => {
            resolve(movement)
        })
        .catch(err => reject(err))
    })
}

function getById(id){
    return new Promise((resolve, reject) =>{
        Movement.find({"name": {$eq : id}})
        .then(movement => {
            resolve(movement)
        })
        .catch(err => reject(err))
    })
}

function getAll(){
    return new Promise((resolve, reject) => {
        resolve(Movement.find())
    })
}

function update(req){
    const name = req.body.name;
    const country = req.body.country;
    const leader = req.body.leader;
    const zones = [];
    const description = req.body.description;
    const state = req.body.state;

    const get_id = req.body.name;
    return new Promise((resolve, reject) =>{
        Movement.find({"name":{$eq : get_id}})
        .then(movement => {
            if(movement.length > 0){
                Person.update(
                    {"name":{$eq : get_id}},
                    {
                        $set:{
                            "name" : name,
                            "country": country,
                            "leader": leader,
                            "description": description,
                            "state": state
                        }
                    }
                );
            }
            else{
                resolve({status: "invalid movement", message: "Movement does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function disable(req){
    const get_id = req.body.name;

    return new Promise((resolve, reject) =>{
        Movement.find({"name":{$eq : get_id}})
        .then(movement => {
            if(movement.length > 0){
                Movement.update(
                    {"id":{$eq : get_id}},
                    {
                        $set:{ "state" : "inactive"}
                    }
                );
                resolve("Movement has been deactivated");
                reject(err => err);
            }
            else{
                resolve({status: "unknown movement", message: "Movement does not exists"})
            }
        })
        .catch(err => reject(err))
    })
}

function addZone(req){
    const name = req.body.name; //movimiento
    const zone = req.body.zone; //zona

    return new Promise((resolve, reject) =>{
        getById(name)
        .then(result =>{
            if(result.length > 0){
                zone.getById(zone)
                .then(res => {
                    if(res.length > 0){
                        Movement.update(
                            {"name":{$eq : name}},
                            {
                                $zones:{
                                    res: _id
                                }
                            }
                        );
                        resolve({status: "Zone added", message: "Zone has been added to the movement"})
                    }
                    else{
                        resolve({status: "invalid zone", message: "Zone does not exist"})
                    }
                })
            }
            else{
                resolve({status: "invalid movement", message: "Movement does not exist"})
            }
        })
    })
}

module.exports = {
    create,
    get,
    getAll,
    update,
    disable,

    addZone
}