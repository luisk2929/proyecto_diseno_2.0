//Router para los queries a la base de datos

import express from 'express';
const router = express.Router();

import {create} from '../controllers/branchController';
import {get} from '../controllers/branchController';
import {update} from '../controllers/branchController';
import {disable} from '../controllers/branchController';
import {getAll} from '../controllers/branchController';
import {addGroup} from '../controllers/branchController';
import {addBoss} from '../controllers/branchController';


router.post('/create', create);
router.get('/get', get);
router.post('/update', update);
router.post('/disable', disable);
router.get('/getAll', getAll);
router.get('/addGroup', addGroup);


export { router as branchRouter }; //?nombre tentativo
