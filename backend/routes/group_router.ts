//Router para los queries a la base de datos

import express from 'express';
const router = express.Router();

import {create} from '../controllers/groupController';
import {get} from '../controllers/groupController';
import {update} from '../controllers/groupController';
import {disable} from '../controllers/groupController';
import {getAll} from '../controllers/groupController';
import { addToBranch } from '../controllers/groupController';
import { assignMonitor } from '../controllers/groupController';
import { addMember } from '../controllers/groupController';
import { getMembers } from '../controllers/groupController';


router.post('/create', create);
router.get('/get', get);
router.post('/update', update);
router.post('/disable', disable);
router.get('/getAll', getAll);
router.post('/addToBranch', addToBranch);
router.post('/assignMonitor', assignMonitor);
router.post('/addMember', addMember);
router.post('/getMembers', getMembers);


export { router as groupRouter }; //?nombre tentativo
