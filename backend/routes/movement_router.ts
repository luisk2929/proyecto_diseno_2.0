//Router para los queries a la base de datos

import express from 'express';
const router = express.Router();

import {create} from '../controllers/movementController';
import {get} from '../controllers/movementController';
import {update} from '../controllers/movementController';
import {disable} from '../controllers/movementController';
import {getAll} from '../controllers/movementController';
import {addZone} from '../controllers/movementController';


router.post('/create', create);
router.get('/get', get);
router.post('/update', update);
router.post('/disable', disable);
router.get('/getAll', getAll);
router.get('/addZone', addZone);


export { router as movementRouter }; //?nombre tentativo
