//Router para los queries a la base de datos

import express from 'express';
const router = express.Router();
const dao = require('../dao/member.dao');

import { login } from '../controllers/loginController';


router.post('/login', login);


export { router as loginRouter }; //?nombre tentativo
