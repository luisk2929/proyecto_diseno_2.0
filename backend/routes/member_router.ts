//Router para los queries a la base de datos

import express from 'express';
const router = express.Router();
const dao = require('../dao/member.dao');

import {create} from '../controllers/memberController';
import {get} from '../controllers/memberController';
import {update} from '../controllers/memberController';
import {disable} from '../controllers/memberController';
import {getAll} from '../controllers/memberController';


router.post('/create', create);
router.get('/get', get);
router.post('/update', update);
router.post('/disable', disable);
router.get('/getAll', getAll);


export { router as memberRouter }; //?nombre tentativo
