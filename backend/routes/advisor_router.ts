//Router para los queries a la base de datos

import express from 'express';
const router = express.Router();

import {create} from '../controllers/advisorController';
import {get} from '../controllers/advisorController';
import {update} from '../controllers/advisorController';
import {disable} from '../controllers/advisorController';
import {getAll} from '../controllers/advisorController';


router.post('/create', create);
router.get('/get', get);
router.post('/update', update);
router.post('/disable', disable);
router.get('/getAll', getAll);


export { router as advisorRouter }; //?nombre tentativo
