//Router para los queries a la base de datos

import express from 'express';
const router = express.Router();

import {create} from '../controllers/zoneController';
import {get} from '../controllers/zoneController';
import {update} from '../controllers/zoneController';
import {disable} from '../controllers/zoneController';
import {getAll} from '../controllers/zoneController';
import {addBranch} from '../controllers/zoneController';
import {addBoss} from '../controllers/zoneController';


router.post('/create', create);
router.get('/get', get);
router.post('/update', update);
router.post('/disable', disable);
router.get('/getAll', getAll);
router.get('/addBranch', addBranch);
router.get('/addBoss', addBoss)


export { router as zoneRouter }; //?nombre tentativo
