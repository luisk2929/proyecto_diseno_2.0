const mongoose = require('mongoose')
const Schema = mongoose.Schema

const GeneralAdvisor = new Schema(
    {
        id:         { type: String, required: true },
        initPeriod: { type: Date, required: true},
        endPeriod:  { type: Date, required: true},
        username:   { type: String, required: true},
        password:   { type: String, required: true},
        status:     { type:String, required: true}
    },
    { timestamps: true },
)

module.exports = mongoose.model('general', GeneralAdvisor)