const mongoose = require('mongoose')
const State = require('./State')
const Schema = mongoose.Schema

const County = new Schema(
    {
        name:       { type: String, required: true },
        decirption: { type: String, required: true },
        state:      { type: String, required: true}
    },
    { timestamps: true },
)

module.exports = mongoose.model('county', County)