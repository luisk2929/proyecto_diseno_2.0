const mongoose = require('mongoose')
const District = require('./District')
const Schema = mongoose.Schema

const Person = new Schema(
    {
        name:       { type: String, required: true },
        id:         { type: String, required: true },
        number:     { type: String, required: true },
        email:      { type: String, required: true },
        district:   { type: String, required: true },
    },
    { timestamps: true },
)

module.exports = mongoose.model('persons', Person)