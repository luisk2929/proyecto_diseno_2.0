const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Group = new Schema(
    {
        name:       { type: String, required: true },
        members:    [ {type: String}],
        monitor:    { type: String, required: true },
        hasBranch:  { type: Boolean, required: true },
        state:      { type: String, required: true}
    },
    { timestamps: true },
)

module.exports = mongoose.model('groups', Group)