const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Branch = new Schema(
    {
        name:       { type: String, required: true },
        leaders:    [ {type: String}],
        groups:     [ {type: String}],
        hasZone:    { type: Boolean, required: true },
        state:      { type: String, required: true },
    },
    { timestamps: true },
)

module.exports = mongoose.model('branch', Branch)