const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Movement = new Schema(
    {
        name:       { type: String, required: true },
        country:    { type: String, required: true },
        leader:     { type: String, required: true },
        zones:      [ {type: String, required: true}],
        description:{ type: String, required: true},
        state:      { type: String, required: true}
    },
    { timestamps: true },
)

module.exports = mongoose.model('movement', Movement)