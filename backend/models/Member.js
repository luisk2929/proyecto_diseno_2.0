const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Member = new Schema(
    {
        state:      { type: String, required: true },
        position:   { type: String, required: true },
        id:         { type: String, required: true },
        group:      { type: String},
    },
    { timestamps: true },
)

module.exports = mongoose.model('member', Member)