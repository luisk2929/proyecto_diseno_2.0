const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Zone = new Schema(
    {
        name:       { type: String, required: true },
        leaders:    [{type: String}],
        branches:   [{type: String}],
        hasMovement:    { type: Boolean, required: true },
        state:      { type: String, required: true}
    },
    { timestamps: true },
)

module.exports = mongoose.model('zones', Zone)