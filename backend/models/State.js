const mongoose = require('mongoose')
const Schema = mongoose.Schema

const State = new Schema(
    {
        name:       { type: String, required: true },
        decirption: { type: String, required: true },
    },
    { timestamps: true },
)

module.exports = mongoose.model('states', State)