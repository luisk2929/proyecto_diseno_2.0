const mongoose = require('mongoose')
const County = require('./County')
const Schema = mongoose.Schema

const District = new Schema(
    {
        name:       { type: String, required: true },
        decirption: { type: String, required: true },
        county:     { type: String, required: true}
    },
    { timestamps: true },
)

module.exports = mongoose.model('district', District)