import React from 'react';
import {Button, Form } from "react-bootstrap";
import axios from "axios";

class New_Person extends React.Component{

    state = {
        form: {
            name : '',
            id : '',
            number : '',
            email : '' ,
            district : 'undefined',
            state: 'active',
            position: 'member'
        }

    }

    handleChange = e => {
        this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })
    }


    submitBtn = (event) =>{
        event.preventDefault(); 
        
       try{
        console.log('state' ,JSON.stringify(this.state.form));
 
        axios.post('http://localhost:5000/member/create', this.state.form)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
       }catch(e){
            console.log(e);
       }
    };

    render(){
        console.log('State:', this.state.form);

        return(
            <div class="container p-8 py-5 my-3 bg-dark text-white mt-5">
            <Form>
                <h1>Add member details</h1>
                <hr color='white'></hr>
                <Form.Group controlId="forID" onChange= {this.handleChange} >
                    <Form.Label >ID</Form.Label>
                    <Form.Control type="ID" placeholder="Enter ID" name = 'id'/>
                </Form.Group>

                <Form.Group controlId="formLastName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="LastName" placeholder="Enter name" onChange= {this.handleChange} name = 'name'/>
                </Form.Group>

                <Form.Group >
                    <Form.Label>Phone number</Form.Label>
                    <Form.Control type="email" placeholder="Enter number" onChange= {this.handleChange} name = 'number'/>
                </Form.Group>

                <Form.Group >
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email address" onChange= {this.handleChange} name = 'email' />
                </Form.Group>

                <Form.Group controlId="formProvince">
                <Form.Label>Province</Form.Label>
                    <select name="Province" class="custom-select">
                        <option selected>Cargar de la base de datos</option>
                    </select>
                </Form.Group>

                <Form.Group controlId="formCanton">
                <Form.Label>Canton</Form.Label>
                    <select name="canton" class="custom-select">
                        <option selected>Cargar de la base de datos</option>
                        <option value="volvo">Volvo</option>
                    </select>
                </Form.Group>

                <Form.Group controlId="formDistrito">
                <Form.Label>Distrit</Form.Label>
                    <select name="distrito" class="custom-select">
                        <option selected>Cargar de la base de datos</option>
                    </select>
                </Form.Group>

                <Button variant="primary" type="submit" onClick = {this.submitBtn}>
                    Submit
                 </Button>
                </Form>
            </div>
        );
    }
}
export default New_Person;
