import React from 'react';


class Navbar extends React.Component{
    render(){
        return(
            <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
              <a className="navbar-brand" href="#">Welcome</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link" href="/Organization">Organization</a>
                    </li>
                    
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navadd" data-toggle="dropdown"> Add Moviments </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a className="dropdown-item" href="/New_movement">New Movement</a>
                            <a className="dropdown-item" href="/New_Zone">New Zone</a>
                            <a className="dropdown-item" href="/New_Branch">New Brach</a>
                            <a className="dropdown-item" href="/New_group">New Group</a>
                            <a className="dropdown-item" href="/New_Person">New Person</a>
                        </div>
                    </li>
                

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navedit" data-toggle="dropdown"> Edit Moviments </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/Edit_movements">Edit movement</a>
                            <a class="dropdown-item" href="/Edit_Zone">Edit Zone</a>
                            <a class="dropdown-item" href="/Edit_Branch">Edit Brach</a>
                            <a class="dropdown-item" href="/Edit_group">Edit Group</a>
                            <a class="dropdown-item" href="/Edit_Person">Edit Person</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navdelete" data-toggle="dropdown"> Delete Moviments </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/Delete_movements">Delete movement</a>
                            <a class="dropdown-item" href="/Delete_zone">Delete Zone</a>
                            <a class="dropdown-item" href="/Delete_branch">Delete Brach</a>
                            <a class="dropdown-item" href="/Delete_group">Delete Group</a>
                            <a class="dropdown-item" href="/Delete_person">Delete Person</a>
                        </div>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link" href="/Log_in">Log in</a>
                    </li>
                    </ul>
                </div>
          </nav>
        );
    }
}

export default Navbar;