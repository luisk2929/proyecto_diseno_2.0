import React from 'react';
import {Button, Form } from "react-bootstrap";
import axios from "axios";



class New_movements extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            persons:[], 
            form: {
                name: '',
                country: 'Costa Rica',
                leader: 'Undefined',
                description: '',
                state: 'Active',

            }
        }
    }
    
    fileSelectedHandler = event => {
        console.log(event);
    }
    componentDidMount(){
        axios.get('http://localhost:5000/member/getAll')
            .then(response=> {
                console.log(response)
                const jperson = JSON.stringify(response.data);
                this.setState({persons: response.data.result})
            })
            .catch(error => {
                console.log(error)
            })
    }



    handleChange = e => {
        this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })
    }

    submitBtn = (event) =>{
        event.preventDefault(); 
        
       try{
        console.log('state' ,JSON.stringify(this.state.form));
 
        axios.post('http://localhost:5000/movement/create', this.state.form)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
       }catch(e){
            console.log(e);
       }
    };
    
    render(){
        const { persons } = this.state
        return(
            <div class="container p-8 py-5 my-3 bg-dark text-white mt-5">

            <Form>  
                <h1>Add details of movement:</h1>
                <hr color='white'></hr>

                <Form.Group controlName="forNameMovement" onChange= {this.handleChange}>
                    <Form.Label>Corporation identification</Form.Label>
                    <Form.Control type="ID" placeholder="Corporation ID" />
                </Form.Group>


                <Form.Group controlName="forNameMovement" onChange= {this.handleChange}>
                    <Form.Label>Name of movement</Form.Label>
                    <Form.Control type="Name" placeholder="Enter Name" name = 'name' />
                </Form.Group>
                

                <Form.Group controlId="formWebsite" onChange= {this.handleChange}>
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="description" placeholder="Enter description"/>
                </Form.Group>          

                <Form.Group controlId="formCountry" onChange= {this.handleChange}>
                <Form.Label>Country</Form.Label>
                    <select name="Country" class="custom-select">
                        <option selected>Costa Rica</option>
                        <option value="volvo">Panama</option>
                    </select>
                </Form.Group>


               
                <div class="container">
                    <h2>Select a counselor for your movement</h2>
                    <p>Members available in the system:</p>      
                    <hr color='white'></hr>      
                    <table class="table table-bordered">
                        <thead>
                            <tr class="table-light">
                            <th>Check</th>
                            <th>Name</th>
                            <th>ID</th>
                            <th>Email</th>
                            <th>Number</th>
                            <th>District</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            persons.length ?
                            persons.map(person =>                
                                    <tr key={person.id} class="table-light">
                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" />
                                                <label class="form-check-label" for="defaultCheck1">
                                                    Add
                                                </label>
                                            </div>
                                        </td>
                                        <td>{person.name}</td>
                                        <td>{person.id}</td>
                                        <td>{person.email}</td>
                                        <td>{person.number}</td>
                                        <td>{person.district}</td>
                                    </tr>
                            ):
                            null
                        }
                                    </tbody>
                    </table>
                </div>

                <Button variant="primary" type="submit" onClick = {this.submitBtn}>
                    Submit
                 </Button>
                </Form>
                
            </div>



        );
    }
}

export default New_movements;
