import React from 'react';


class List_of_movements extends React.Component{
    render(){
        return(
            <div className = "container py-5">
                <div class="jumbotron">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Movement 1</h4>
                        <p class="card-text">Description.</p>
                        <a href="#" class="card-link">link</a>
                    </div>
                </div>

                <div class="card " >
                    <div class="card-body">
                        <h4 class="card-title">Movement 2</h4>
                        <p class="card-text">Description.</p>
                        <a href="#" class="card-link">link</a>
                    </div>
                </div>
                    
                </div>
                <a href="/New_movement" class="btn btn-info" role="button">Make new movement</a>

            </div>
        );
    }
}
export default List_of_movements;
