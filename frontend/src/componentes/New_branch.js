import React from 'react';

import {Button, Form } from "react-bootstrap";
import axios from "axios";
class New_branch extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            persons:[], 
            movements:[], 
            zone:[], 
            form: {
                name: '',
                state: 'Activo',
            }
        }
    }

    fileSelectedHandler = event => {
        console.log(event);
    }
    componentDidMount(){
        axios.get('http://localhost:5000/member/getAll')
            .then(response=> {
                console.log(response)
                this.setState({persons: response.data.result})
            })
            .catch(error => {
                console.log(error)
            })
        axios.get('http://localhost:5000/movement/getAll')
            .then(response=> {
                console.log(response)
                this.setState({movements: response.data.result})
            })
            .catch(error => {
                console.log(error)
            })
        axios.get('http://localhost:5000/zone/getAll')
            .then(response=> {
                console.log(response)
                this.setState({zone: response.data.result})
            })
            .catch(error => {
                console.log(error)
            })
    }
    
    handleChange = e => {
        this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })
    }

    submitBtn = (event) =>{
        event.preventDefault(); 
        
       try{
        console.log('state' ,JSON.stringify(this.state.form));
 
        axios.post('http://localhost:5000/branch/create', this.state.form)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
       }catch(e){
            console.log(e);
       }
    };

    render(){
        const { persons } = this.state
        const { movements } = this.state
        const { zone } = this.state
        return(
            <div class="container p-8 py-5 my-3 bg-dark text-white mt-5">
            <Form>
                <h1>Add details of branch:</h1>
                <hr color='white'></hr>
                <Form.Group controlName="forNameBranch"  onChange= {this.handleChange}>
                    <Form.Label>Name branch</Form.Label>
                    <Form.Control type="Branch" placeholder="Enter Name" name='name'/>
                </Form.Group>

                <Form.Group  onChange= {this.handleChange}>
                    <Form.Label>ID Branch</Form.Label >
                    <Form.Control type="Branch" placeholder="Enter Id Branch" name='id'/>
                </Form.Group>

                <Form.Group controlId="formType"  onChange= {this.handleChange}>
                <Form.Label>Type</Form.Label>
                    <select name="Type" class="custom-select">
                    <option value="volvo">Asociación</option>
                        <option value="volvo"></option>
                    </select>
                </Form.Group>

                <Form.Group controlId="formMovement"  onChange= {this.handleChange}>
                <Form.Label>Choose the movement that belongs</Form.Label>
                    <select name="Movement" class="custom-select">
                    {
                            movements.length ?
                            movements.map(movement =>                
                                    <option value="volvo"> {movement.name} </option>
                            ):
                            null
                        }
                        
                    </select>
                </Form.Group>

                <Form.Group controlId="formMovement"  onChange= {this.handleChange}>
                <Form.Label>Choose the zone that belongs</Form.Label>
                    <select name="Movement" class="custom-select">
                    {
                            zone.length ?
                            zone.map(zone =>                
                                    <option value="volvo"> {zone.name} </option>
                            ):
                            null
                        }
                        
                    </select>
                </Form.Group>
            

                <div class="container">
                    <h2>Select a counselor for your movement</h2>
                    <p>Members available in the system:</p>      
                    <hr color='white'></hr>      
                    <table class="table table-bordered">
                        <thead>
                            <tr class="table-light">
                            <th>Check</th>
                            <th>Name</th>
                            <th>ID</th>
                            <th>Email</th>
                            <th>Number</th>
                            <th>District</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            persons.length ?
                            persons.map(person =>                
                                    <tr key={person.id} class="table-light">
                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" />
                                                <label class="form-check-label" for="defaultCheck1">
                                                    Add
                                                </label>
                                            </div>
                                        </td>
                                        <td>{person.name}</td>
                                        <td>{person.id}</td>
                                        <td>{person.email}</td>
                                        <td>{person.number}</td>
                                        <td>{person.district}</td>
                                    </tr>
                            ):
                            null
                        }
                                    </tbody>
                    </table>
                </div>

                <Button variant="primary" type="submit" onClick = {this.submitBtn}>
                    Submit
                 </Button>
                </Form>
            </div>
        );
    }
}
export default New_branch;
