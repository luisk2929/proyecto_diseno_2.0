
import Navbar from './componentes/Navbar'
import Log_in from './componentes/Log_in';
import New_Person from './componentes/New_Person'
import List_of_movements from './componentes/List_of_movements'
import New_movement from './componentes/New_movement'
import New_group from './componentes/New_group'
import New_zone from './componentes/New_zone'
import New_branch from './componentes/New_branch'

import Edit_movements from './componentes/Edit_movements'
import Edit_zone from './componentes/Edit_zone'
import Edit_branch from './componentes/Edit_branch'
import Edit_group from './componentes/Edit_group'
import Edit_person from './componentes/Edit_person'

import Delete_movements from './componentes/Delete_movements'
import Delete_zone from './componentes/Delete_zone'
import Delete_branch from './componentes/Delete_branch'
import Delete_group from './componentes/Delete_groups'
import Delete_person from './componentes/Delete_person'

import './css/App.css'
import './includes/bootstrap';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div>
      <Router>
        <Navbar />
        <Switch>

          <Route path="/Organization"> <List_of_movements /> </Route>

          <Route path="/New_movement"> <New_movement /> </Route>
          <Route path="/New_Zone">     <New_zone />     </Route>
          <Route path="/New_Branch">   <New_branch />   </Route>
          <Route path="/New_Person">   <New_Person />   </Route>
          <Route path="/New_group">    <New_group />    </Route>


          <Route path="/Edit_movements"> < Edit_movements />  </Route>
          <Route path="/Edit_zone">      < Edit_zone />  </Route>
          <Route path="/Edit_branch">    < Edit_branch />  </Route>
          <Route path="/Edit_group">     < Edit_group />  </Route>
          <Route path="/Edit_person">    < Edit_person />  </Route>

          <Route path="/Delete_movements"> < Delete_movements />  </Route>
          <Route path="/Delete_zone">      < Delete_zone />  </Route>
          <Route path="/Delete_branch">    < Delete_branch />  </Route>
          <Route path="/Delete_group">     < Delete_group />  </Route>
          <Route path="/Delete_person">    < Delete_person />  </Route>
    
          <Route path="/Log_in"> <Log_in /> </Route>
          <Route path="/"> </Route>

          
        </Switch>
      </Router>

       
    </div>
   
  );
}

export default App;
